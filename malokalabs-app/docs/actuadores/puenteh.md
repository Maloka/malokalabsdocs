# Puente H

![l298mini](../img/actuadores/puenteH/l298mini.jpg)

## Ejemplo de conexión

### Esquema de conexión

![puenteH](../img/actuadores/puenteH/puenteH_bb.png)

### Tabla de verdad un motor

|In1|In2|Motor|
|:-------------:|:-------------:|:-----:|
|0|0|Detenido|
|0|1|Giro ↑↑↑|
|1|0|Contra giro ↓↓↓|
|1|1|Indebido|

### Tabla de verdad dos motores en un carro

|In1|In2|In3|In4|MotorA|MotorA|Móvil|
|:----:|:----:|:----:|:----:|:-------:|:-------:|:-------:|
|0|0|0|0|Detenido|Detenido|sin avanzar|
|0|1|0|1|Giro ↑↑↑|Giro ↑↑↑|Adelante|
|0|1|1|0|Giro ↑↑↑|Contra giro ↓↓↓|Rota derecha|
|1|0|0|1|Contra giro ↓↓↓|Giro ↑↑↑|Rota izquierda|
|0|1|0|0|Giro ↑↑↑|Detenido|Gira derecha|
|0|0|0|1|Detenido|Giro ↑↑↑|Gira izquierda|
|1|0|1|0|Contra giro ↓↓↓|Contra giro ↓↓↓|Atrás|
|1|1|1|1|Indebido|Indebido|Detenido|

### Materiales

![Elementos puente H](../img/actuadores/puenteH/elementos-puenteh.jpg)


### Vídeo demostrativo

<iframe width="560" height="315" src="https://www.youtube.com/embed/9LSyMVQatQA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Ejemplo de Algoritmo

![puenteH](../img/actuadores/puenteH/puenteH_luabot.jpg)


