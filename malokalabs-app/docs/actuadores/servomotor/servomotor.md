# Servomotor

Dispositivo que tiene un brazo que puede ser posicionado en un ángulo.

* El ángulo puede ser entre [0 - 180] grados
* La fuerza de torsión es de un kilogramo de fuerza.

## Ejemplos

### Cambio de posición manual

Aquí te invitamos a jugar con el ángulo en el que desees que se ponga el brazo, en el ejemplo, el ángulo inicial
es 0 grados, al pasar un segundo el brazo se ubicará en el ángulo de 90 grados donde podrá generar hasta una
fuerza de torsión de un kilogramo, pasado un segundo volverá a su posición inicial, este proceso se repite 4 veces. Juega cambiando el angulo y el tiempo, inclusive
agrega más pasos para que cambie entre más ángulos antes de apagarse.

#### Circuito

![Idea de conexión](servomotor-conexiones.jpeg)

![Pictograma](nodemcu_servomotor_bb.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

#### Programa

![algoritmo](servo-motor-codigo.jpg){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}


