# Kit MalokaLabs (basado Catalejo editor)

![ejemplos de proyectos](./img/index/presentation1.png)

!!! Info "Escucha el audio de bienvenida a la documantación del Kit"
     <audio controls style="width: 100%">
      <source src="offline/audios/audio-introduccion.ogg" type="audio/ogg">
      Your browser does not support the audio element.
    </audio>

:material-spider:**¡Un gran poder conlleva una gran responsabilidad!**:material-spider:

Bienvenida al mundo maker/hacker: curiosea, pregunta, imagina, construye y sueña con este material diseñado para ti.

## ¿Cómo iniciarte? :thinking:

!!! Info "Escucha el audio para indicarte como inciarte con el Kit"

    <audio controls style="width: 100%">
      <source src="offline/audios/como-iniciarte.ogg" type="audio/ogg" >
      Your browser does not support the audio element.
    </audio> 

Realiza la exploración del kit y luego ponte aprueba con los retos que tenemos para ti.

### Exploración del Kit :face_with_monocle:

Para iniciarte en este mundo de makers lo que primero deberás hacer es conocer los componentes que tiene tu kit, para tal fin te invitamos a ir al siguiente enlace realizando los
ejercicios propuestos en el primer vídeo como leyendo la descripción de cada elemento del kit.

[:tools: Enlace para explorar el Kit :tools:](intro/kit/){ .md-button .md-button--primary .heart}

### Retos :exploding_head:

!!! Info "Te invitamos a realizar los siguientes retos que están organizados desde el nivel 0 hasta el nivel 3; comparte tu experiencia en el servidor de Discord"
    Al realizar los retos desde el nivel 0 al nivel 3 podrás:

    - [x] Reconocer la polaridad de los componentes eléctricos
    - [x] Podrás construir circuitos eléctricos (Hardware)
    - [x] Crear y enviar programas a la minicomputadora nodemcu (Software)
    - [x] Controlar tus circuitos eléctricos a través de una página Web
        


[Reto nivel :zero: :vulcan:{.heartBig}](intro/mi-primer-circuito/){ .md-button .md-button--primary} {==Construye tu primer circuito==}

[Reto nivel :one: :fire:{.heartBig}](intro/mi-primer-programa/){ .md-button .md-button--primary} {==Atrévete a crear tu primer programa con el kit==}

[Reto nivel :two: :fire:{.heartBig}:fire:{.heartBig}](intro/holamundo/){ .md-button .md-button--primary} {==Programa tu primer LED con CatalejoEditor/MalokaLabs==}

[Reto nivel :three: :fire:{.heartBig}:fire:{.heartBig}:fire:{.heartBig}](intro/holamundo-http/){ .md-button .md-button--primary} {==Crea tu primera página Web y controla tu kit==}


## Nuestra comunidad

Ésta documentación está en continua construcción te invitamos a participar compartiendo tu
experiencia desarrollando tus proyectos. 

Te invitamos a ser parte de nuestro servidor en discord, dale clic en la imagen:


[:fontawesome-brands-discord: :material-telescope: Catalejo :material-telescope: :fontawesome-brands-discord:](https://discord.gg/4GxYxyy){ .md-button .md-button--primary .heart}

[Enlace de invitación servidor de discord Catalejo](https://discord.gg/4GxYxyy)

[:fontawesome-brands-discord: Chicas STEAM :fontawesome-brands-discord:](https://discord.gg/dgpH6ek8gK){ .md-button .md-button--primary .heart}

[Enlace de invitación servidor de discord Chicas STEAM](https://discord.gg/dgpH6ek8gK)

¡Estaremos felices de leerte!

## Agradecimientos

Agradecemos la colaboración de cada estudiante que ha aportado su grano de arena
para ayudarnos a generar una documentación a ésta valiosa herramienta que hemos denominado
LuaBot.

Atentamente:

<img src="https://s.gravatar.com/avatar/9d9438c3f89fc9ec7c2e26f31f3090e4?s=80">
<p>
Johnny Cubides<br>
e-mail: jgcubidesc@unal.edu.co<br>
discord: johnnycubides#0705<br>
telegram: @johnny5<br>
<p>
