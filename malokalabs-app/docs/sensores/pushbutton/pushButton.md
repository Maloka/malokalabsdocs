# Leyendo pulsadores

En esta oportunidad usaremos un pulsador como entrada de información
para el sistema; al oprimir el botón la tarjeta procesará esa
información y encenderá el LED, al soltar el pulsador, la tarjeta
mandará una señal al LED para que se apaque.

!!! Info "Sino sabes que son los pulsadores te invitamos ver primero este vídeo antes de realizar el experimento"
    

<iframe width="640" height="480" src="https://www.youtube.com/embed/TO2YQIWtOeA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Materiales

Para desarrollar éste ejemplo necesitarás:

|Cantidad|Nombre|Imagen|
|:-------------:|:-------------:|:-----:|
|1 | Pulsador de 2 pines | ![Pulsador de 2 pines](../../img/sensores/pushButton2pines.png){: style="width:30%; background:white;"}|
|1 | [Tarjeta NodeMCU](../../../nodemcu_v1/) con el [software instalado](../../../install/firmware/)| ![Imagen nodemcu v3](../../img/esp8266/nodemcu.png){: style="width:30%; background:white;"}|
|1| Resistencia de 330 Ohms| ![resistencia 220](../../img/basic/resistencia330.png){: style="width:30%; margin-left: auto; margin-right: auto; display: block"}|
|1| Resistencia de 10 KOhms| ![resistencia 10k](../../img/basic/resistance10k.jpg){: style="width:30%; margin-left: auto; margin-right: auto; display: block"}|
|1| LED de cualquier color| ![led](../../img/actuadores/led-difuso.png){: style="width:30%; margin-left: auto; margin-right: auto; display: block"}
|4| Conector rápido (Jumper) macho-macho| ![conector rápido m-m](../../img/basic/jumper-m-m.png){: style="width:30%; margin-left: auto; margin-right: auto; display: block"}|
|1| Tablero de prototipos (Protoboard)| ![protoboard](../../img/basic/protoboardHalf.png){: style="width:30%; margin-left: auto; margin-right: auto; display: block"}|
|1 | Cable USB micro B| ![cable USB](../../img/accesories/usb-micro-b.jpg){: style="width:30%;"}|
|1 | Cargador de celular o [baterías](../../../intro/energize-card/) (PowerBank) | ![cargador de celular](../../img/accesories/charger-5v.png){: style="width:30%; margin-left: auto; margin-right: auto; display: block"}|

## Creando nuestro circuito

Lo primero que debes hacer es imitar de la siguiente imagen la
manera y los lugares donde se conecta cada elemento del circuito
a armar, comprobando las veces que sea necesario nuestro montaje
con el diagrama pictografico, ¡Manos a la obra!

![Ejemplos de conexión](nodemcu_pushbutton_bb.png)

## Construyamos nuestro programa

Abre la aplicación *Catalejo Editor* juega un poco con ella
buscando los bloques de programación de código y cuando la hayas explorado
lo suficiente trata de armar el siguiente código de bloques,
revisa muchas veces que te quede igual.

![Ejemplo de programa](pushButtonPrograma.jpeg)

## Probando el funcionamiento

Envía el programa a la minicomputadora NodeMCU y reinicia con el botón RTS.
Ahora para probar su funcionamiento deberás oprimir el pulsador, apenas lo hagas,
el LED deberá enceder y al soltar el botón deberá apagarse.

## Preguntas para analizar

!!! Note "Piensa en las siguientes preguntas y trata de pensar cómo lo podrías hacer"
    
    - [x] ¿Cómo podrías agregar otro LED y otro pulsador?


Desde luego hay detalles que se te escaparán y muchas dudas
tendrás; ten presente que es difícil representar todas las
respuestas en ese par de imágenes, te a invitamos usar nuestro 
servidor de 
[Discord](https://discord.gg/4GxYxyy)
o
[nuestro foro](https://catalejo.flarum.cloud/t/catalejo-luabot-nodemcu)
para hacer esas preguntas que te quedan, estaremos prestos a apoyarte.
