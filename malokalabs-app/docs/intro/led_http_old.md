# Encendido de LED a través de WIFI

Si quiere entender mejor el proceso de conexión entre LuaBot y
su celular, se le recomienda ver el siguiente vídeo

<iframe width="560" height="315" src="https://www.youtube.com/embed/x1eC3EHYvqQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Ejemplo de Conexión

![led_http](../img/led_http/led_on_off_http_bb.png)


## Ejemplo de algoritmo

Cortesía de:
<p style="color:blue;">Isabella <br>discord: IsabellaRopain#8294</p>

![led_http](../img/led_http/led_http_luabot.png)



