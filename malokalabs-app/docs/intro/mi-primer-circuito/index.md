# Mi primer LED

![mi primer led programado.png](mi-primer-led-programado.png)

## Audio de apoyo sobre el reto

!!! Info "Este reto es muy importante, te invitamos a escuchar las instrucciones en el siguiente audio"
    
    <audio controls style="width: 100%">
      <source src="../../offline/audios/mi-primer-circuito.ogg" type="audio/ogg">
      Your browser does not support the audio element.
    </audio>

## Antes de realizar el reto:

!!! Info "Antes de realizar tu primer circuito te invitamos a realizar la exploración del kit, además, para que comprendas como iniciarte con el kit te invitamos a revisar el siguiente vídeo:"

<iframe width="640" height="480" src="https://www.youtube.com/embed/x8VLAdykznw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## ¡Hora de realizar el reto!

Realizada la exploración del kit te invitamos a realizar tu "primer circuito", por favor revisa la siguiente presentación donde aprenderás claves para realizar tus circuitos (hardware).

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTvLLsCm1fsEnkwC1YwjzbprqyGAIcMqm6TXuMq4ecQjRQxNjRGTjV1Y7tmhh7C8vgjEf9FYjKske9l/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<!-- <iframe src="/circuitjs/circuitjs.html?ctz=CQAgzCAMB0l3BWEBGaCBsAWATOsyAOAzQg5AdmxAQE5rrJqBTAWmWQCgAnEG89EJkwFe-akKjgwkDgA9eRQZRSQESTOXUoqAJSYBnAJb6ALkwB2AY0MBDDsnRU+AsAidiSAqgBMmAMxsAVwAbExZgpm8USUYYSE55GhoXGmQVZDowGkxBbRAAGQBRABEOADcQcmJBLErqzxjBRjBoCFioNA4AdzqczFqqnIQJGR7B8RznCahu0QEhESmG0bnwN161qhXx1ypx5Y4Acw3hnPGMdpl5dD5BHEq1O61kKgA5AHtfAFkAYQBVOSVW44RgEXCCbAQHIvEAAHQADrD9GAAGqAghwGoCMjkGp0aFUFhIgDiHCAA" width="800" height="auto"></iframe> -->

## Simulación

A continuación encontrarás la simulación del circuito que acabas de realizar, te invitamos a que modifiques el valor de la resistencia dando en el componente doble clic, analiza que sucede con el LED y la corriente al cambiar aquellos
valores.

<iframe src="https://catalejo_team.gitlab.io/catalejo-dev/catalejo-luabot-esp8266-docs/circuitjs/circuitjs.html?ctz=CQAgzCAMB0l3BWEBGaCBsAWATOsyAOAzQg5AdmxAQE5rrJqBTAWmWQCgAnEG89EJkwFe-akKjgwkDgA9eRQZRSQESTOXUoqAJSYBnAJb6ALkwB2AY0MBDDsnRU+AsAidiSAqgBMmAMxsAVwAbExZgpm8USUYYSE55GhoXGmQVZDowGkxBbRAAGQBRABEOADcQcmJBLErqzxjBRjBoCFioNA4AdzqczFqqnIQJGR7B8RznCahu0QEhESmG0bnwN161qhXx1ypx5Y4Acw3hnPGMdpl5dD5BHEq1O61kKgA5AHtfAFkAYQBVOSVW44RgEXCCbAQHIvEAAHQADrD9GAAGqAghwGoCMjkGp0aFUFhIgDiHCAA" width="800" height="500"></iframe>

<a href="https://catalejo_team.gitlab.io/catalejo-dev/catalejo-luabot-esp8266-docs/circuitjs/circuitjs.html?ctz=CQAgzCAMB0l3BWEBGaCBsAWATOsyAOAzQg5AdmxAQE5rrJqBTAWmWQCgAnEG89EJkwFe-akKjgwkDgA9eRQZRSQESTOXUoqAJSYBnAJb6ALkwB2AY0MBDDsnRU+AsAidiSAqgBMmAMxsAVwAbExZgpm8USUYYSE55GhoXGmQVZDowGkxBbRAAGQBRABEOADcQcmJBLErqzxjBRjBoCFioNA4AdzqczFqqnIQJGR7B8RznCahu0QEhESmG0bnwN161qhXx1ypx5Y4Acw3hnPGMdpl5dD5BHEq1O61kKgA5AHtfAFkAYQBVOSVW44RgEXCCbAQHIvEAAHQADrD9GAAGqAghwGoCMjkGp0aFUFhIgDiHCAA"> Simulación de circuito mi primer LED</a>

## Herramientas

* [Calculadora de resistencias de carbón 4 bandas](https://www.digikey.com/es/resources/conversion-calculators/conversion-calculator-resistor-color-code)
