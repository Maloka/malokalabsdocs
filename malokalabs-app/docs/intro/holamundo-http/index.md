# Encendido de LED a través de WIFI

![led_http](baner-blink-http.jpeg)

## Audio de apoyo sobre el reto

!!! Info "Escucha las recomendaciones dadas para este reto en particular en el siguiente audio"

    <audio controls style="width: 100%">
      <source src="../../offline/audios/pagina-web.ogg" type="audio/ogg">
      Your browser does not support the audio element.
    </audio>
    

## Antes de realizar el reto:

!!! Info "Antes de realizar este ejemplo deberás realizar tu [primer programa con Catalejo Editor/MalokaLabs, visita este enlace](../mi-primer-programa/)"

## Materiales

Para desarrollar éste ejemplo necesitarás:

|Cantidad|Nombre|Imagen|
|:-------------:|:-------------:|:-----:|
|1 | [Tarjeta NodeMCU](../../../nodemcu_v1/) con el [software instalado](../../../install/firmware/)| ![Imagen nodemcu v3](../../img/esp8266/nodemcu.png){: style="width:30%; background:white;"}|
|1| Resistencia de 330 Ohms | ![resistencia 220](../../img/basic/resistencia330.png){: style="width:30%; margin-left: auto; margin-right: auto; display: block"}|
|1| LED de cualquier color| ![led](../../img/actuadores/led-difuso.png){: style="width:30%; margin-left: auto; margin-right: auto; display: block"}
|1| Conector rápido (Jumper) macho-macho| ![conector rápido m-m](../../img/basic/jumper-m-m.png){: style="width:30%; margin-left: auto; margin-right: auto; display: block"}|
|1| Tablero de prototipos (Protoboard)| ![protoboard](../../img/basic/protoboardHalf.png){: style="width:30%; margin-left: auto; margin-right: auto; display: block"}|
|1 | Cable USB micro B| ![cable USB](../../img/accesories/usb-micro-b.jpg){: style="width:30%;"}|
|1 | Cargador de celular o [baterías](../../../intro/energize-card/) (PowerBank) | ![cargador de celular](../../img/accesories/charger-5v.png){: style="width:30%; margin-left: auto; margin-right: auto; display: block"}|

## Creando nuestro circuito

Lo primero que debes hacer es imitar de la siguiente imagen la
manera y los lugares donde se conecta cada elemento del circuito
a armar, comprobando las veces que sea necesario nuestro montaje
con el diagrama pictografico, ¡Manos a la obra!

![Ejemplos de conexión](../holamundo/nodemcu_led_v2_bb.png)

Si requieres apoyo en el montaje de este circuito puedes guiarte a través del siguiente vídeo.

<iframe width="560" height="315" src="https://www.youtube.com/embed/sRm5kzA9zCc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Construyamos nuestro programa

Abre la aplicación *Catalejo Editor/MalokaLabs* juega un poco con ella
buscando los bloques de programación de código y cuando la hayas explorado
lo suficiente trata de armar el siguiente código de bloques,
revisa muchas veces que te quede igual.

Cortesía de:
<p style="color:blue;">Isabella Ropero, Chica STEAM 2020, Maloka <br>discord: IsabellaRopain#8294</p>

![Ejemplo de programa](led-http-catalejo-editor.png)

Si requieres apoyo para ubicar los bloques para la programación en tu aplicación te inviatmos a ver el siguiente video.

<iframe width="560" height="315" src="https://www.youtube.com/embed/lnx83WAi-3g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Desde luego hay detalles que se te escaparán y muchas dudas
tendrás; ten presente que es difícil representar todas las
respuestas en ese par de imágenes, para ello te invitamos
a ver el siguiente vídeo que despejará unas dudas y desde
luego generará otras. No olvides que puedes usar nuestro
servidor de 
[Discord](https://discord.gg/4GxYxyy)
o
[nuestro foro](https://catalejo.flarum.cloud/t/catalejo-luabot-nodemcu)
para hacer esas preguntas que te quedan, estaremos prestos a apoyarte.

## Probando nuestro circuito y programa

Después de crear el programa deberás seguir los pasos:

|Pasos | Imagen de ejemplo |
| --- | --- |
| 1. Conéctate a la micro-computadora nodemcu por WiFi; busca una red WiFi llamada catalejo seguida de unos números y letras, ejemplo: **catalejo-85da**, recuerda que la contraseña es **87654321** además si te sale un cuadro de dialogo diciendo que la conexión no tiene internet y preguntado que si deseas continuar dile que sí y continúa, sino aún no sabes qué hacer en este punto te pedimos revisar el ejemplo de [primer programa con Catalejo Editor/MalokaLabs, visita este enlace](../../mi-primer-programa/mi-primer-programa/). | |
| 2. Envia el programa desde la aplicación a la micro-computadora | |
| 3. Oprimir el botón **RST** para reiniciar la tarjeta | ![](../../img/esp8266/nodemcu-v3.png) | 
| 4. Revisa que sigues conectada a la tarjeta desde tu celular o tablet, si te desconectaste vuelve a conectarte a la red **catalejo...** | ![](wifi.jpg) |
| 5. Entra al navegador Web del celular o tablet e introduce la siguiente dirección ip **192.168.4.1**, te deberá aparecer la página web que tú creaste | ![](ip-requerida.jpg) |
| 6. Interactúa con la página web tocando los botones de ON y OFF para encender o apagar el LED | ![](pagina-web.jpeg) |

<!-- ![navegador y dirección ip](pagina-web.jpeg){: style="width:70%; margin-left: auto; margin-right: auto; display: block"} -->

Si requieres más ayuda con respecto a la prueba de la página web, puedes ver el siguiente vídeo:

<iframe width="560" height="315" src="https://www.youtube.com/embed/0mNPwD6mKKk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
