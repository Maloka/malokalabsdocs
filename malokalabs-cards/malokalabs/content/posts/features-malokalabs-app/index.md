---
title:  "¿Cómo usar MalokaLabs?"
img: MalokaLabs.png
categories: ["one", "two"]
date: "2021-07-20"
---

Te explicamos a continuación ¿Cómo instalar la
app de MalokaLabs y como hacer uso de sus
funcionalidades principales como son: Compartir
proyectos, editar y guardar proyectos

## Instalación de MalokaLabs

{{< youtube H4XqGg6om6M >}}

## Proceso de exploración de documentación

{{< youtube ue96o4TBk50 >}}

## Crear código con Blockly

{{< youtube eLEe050KROA >}}

## Abrir y guardar proyectos

{{< youtube OJL4Y2-WM9o >}}

## Compartir proyectos

{{< youtube pdgsAq-B_cw >}}

## Conexión con tarjeta

{{< youtube DNxETgqV2rU >}}
