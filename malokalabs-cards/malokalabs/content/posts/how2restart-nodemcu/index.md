---
title:  "Reiniciar tarjeta"
img: Tarjeta.png
categories: ["one", "two"]
date: "2021-07-19"
---

En este apartado encontrarás información relacionada al reinicio de la tarjeta nodemcu,
esto puede ser importante cuando tu tarjeta no permita nuevamente la programación desde tu celular.

## Reinicio general de la tarjeta NodeMCU

{{<youtube _d-Xt5PduBo>}}
