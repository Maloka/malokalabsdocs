---
title:  "Cacharros que inspiran..."
img: Cacharros.png
categories: ["one", "two"]
date: "2021-12-17"
---

A continuación, te mostramos videos que te pueden inspirar en la creación de tus proyectos con el kit de Chicas STEAM.

## Circuito de señal de audio y luz

{{< youtube Gg459V0I1HA >}}

## Transistor: el gran avance tecnológico

{{< youtube fOoNr6xAzEk >}}

## Explicación de la protoboard y los cables jumper del Kit de Chicas STEAM

{{< youtube x8VLAdykznw >}}

## Transistor montaje

{{< youtube 6WoNaY2LsMI >}}

## El sensor ultrasónico HC-04

{{< youtube NQPdouAhN1g >}}

## ¿Qué son las resistencias?

{{< youtube CuYk5MfqaB8 >}}

## Programación del sensor de agua de lluvia

{{< youtube 0F6Dml0DAd8 >}}

## Diferencia entre pulsadores e interruptores

{{< youtube TO2YQIWtOeA >}}

## ¿Qué onda con los potenciómetros?

{{< youtube iOFSNb3a1wY >}}

## Diodo emisor de luz (LED)

{{< youtube IjmZW9ImkWk >}}

## La fotorresistencia

{{< youtube GaahzsHIGmU >}}

