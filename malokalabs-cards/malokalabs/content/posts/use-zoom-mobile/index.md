---
title:  "Usar Zoom en Android"
img: ZoomAndroid.png
categories: ["one", "two"]
date: "2021-07-17"
---

Te vamos a presentar una ayudas que te pueden servir a mejorar tu experiencia en las sesiones sincrónicas en *Chicas Steam*, comencemos.

## ¿Cómo instalar zoom desde Android?

{{< youtube haJ3_d5To_I >}}

## Poner mi nombre en Zoom desde Android

{{< youtube nRz30bMyZiQ >}}

## Activar micrófono y cámara en Zoom desde Android

{{< youtube gMYFV4IYjqw >}}

## Levantar la mano y reaccionar en Zoom desde Android

{{< youtube xJmQRkEZxhw >}}
