---
title:  "Discord: primeros pasos"
img: Discord.png
categories: ["one", "two"]
date: "2022-03-31"
---

Chicas STEAM te da la oportunidad de interactuar con una gran comunidad de mediadores, mentoras y con más Chicas STEAM que han hecho o hacen
parte del programa a través de chats, canales para vídeo-llamadas y demás contenido de interés que tu podrás alimentar con tus aportes,
te invitamos a hacer parte de esta gran comunidad.

## Invitación al servidor de Discord de Chicas STEAM

Visita el siguiente enlace

{{< discordinvitation dgpH6ek8gK 35 >}}

## ¿No sabes nada del mundo de Discord y quieres aprender?

{{< iframeGooglePres "https://docs.google.com/presentation/d/e/2PACX-1vRlVTCjHT3_NYmxdyOdXr_g6m80_PWaDUnFR3DDU14Cvy3ttGamdsRKid712CTFQSYipgoecmCnOMvU/embed?start=false&loop=false&delayms=3000" >}}

{{< huglink "Enlace al tutorial uso de Discord" "https://docs.google.com/presentation/d/1z8toQDH5hL_xtKE6CSOycE3VZ8V3BhjcgfrnEuRlQxM/edit#slide=id.ge6f279836e_0_51" 40 >}}


# Vídeos de apoyo para aprender a usar Discord

Puedes apoyarte en los siguientes vídeos que te permitirán seguir el paso a paso de todo lo que necesitas para mejorar tu experiencia en Discord.

## Crear cuenta y cambiar avatar

{{<youtube X3Uij-o61mE>}}

## Contenido del servidor y estructura

{{<youtube DqQwHfEWvIQ>}}

## Cambiar nickname e interacción con el servidor

{{<youtube Wh-vH1SWLX0>}}

## Compartir documentos en canal

{{<youtube 9Tx8ws06mII>}}

## Canal de voz

{{<youtube -U0K0aQ3CU0>}}

## Identificación con bot samus

{{<youtube z1GVxtWcCWU>}}
