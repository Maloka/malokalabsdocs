---
title:  "Carpeta Violeta"
img: rutas-atencion.jpeg
categories: ["one", "two"]
date: "2022-03-31"
---

## Rutas de atención de VBG por ciudades

> {{< huglink "Líneas de atención en salud mental y consumo de sustancias psicoactivas" "https://coronaviruscolombia.gov.co/Covid19/aislamiento-saludable/lineas-de-atencion.html" 20 >}}
> Enumera líneas de atención a nivel nacional por departamento. Incluye líneas de atención del estado como líneas comunitarias.

> {{< huglink "Ruta de atención violencias basadas en género" "http://centrodedocumentacion.prosperidadsocial.gov.co/2021/Micrositio/Prevencion-de-la-Violencia/Infografias/Ruta_de_Atencion.pdf" 20 >}}
> Infografía con la ruta de atención institucional nivel nacional.

> {{< huglink "Ruta unica de atención a mujeres victimas de violencia y en riesgo de feminicidio (BOGOTÁ)" "https://rutadeatencion.sdmujer.gov.co/index.html" 20 >}}
> Página web con la ruta de atención de la secretaria de la mujer por localidad.

> {{< huglink "Ruta de atención frente a la vulneración de derechos de los niños, niñas y adolescentes" "https://www.integracionsocial.gov.co/images/_docs/2021/Ruta_atencion.pdf" 20 >}}
> Infografía con la ruta de atención institucional nivel distrital - integración social

> {{< huglink "Ruta de atención a niñas y mujeres victimas de violencia" "https://www.cali.gov.co/mujer/publicaciones/106198/ruta_de_atencion_a_ninas_y_mujeres_victimas_de_violencias/" 20 >}}
> Página web con la ruta de atención de la alcaldía de Cali.

## Manuales de prevención de VBG

> {{< huglink "kit de herramientas para la protección de la infancia y la adolescencia" "https://d25ltszcjeom5i.cloudfront.net/60461/wwaoltgqgi/216_kit_de_herramientas.pdf" 20 >}}
> Es una guia que entrega estrategias y consejos para que como adultos reconozcamos algunas situaciones que son inapropiadaso o violentas hacia los niños, las niñas y adolescentes.

> {{< huglink "Violentómetro" "https://www.ipn.mx/genero/inicio/violentometro-regla.pdf" 20 >}}
> Pieza en forma de regla y con escala de color que indica el tipo de violencias basadas en género y cómo se van o pueden ir incrementando.

> {{< huglink "Acosómetro" "https://www.ipn.mx/genero/materialesdeapoyo/acos%C3%B3metro.html" 20 >}}
> Pieza en forma de regla y con escala de color que permite identificar el acoso como una forma de violencia basada en género.

## ONG y colectiv@s apoyo y acompañamiento

> {{< huglink "La juntanza feminista" "https://www.instagram.com/lajuntanzaescuelaa/" 20 >}}
> Educación en feminismos, derechos humanos y salud mental		

> {{< huglink "Fundación Orientáme" "https://orientame.org.co/" 20 >}}, 
> {{< huglink "Instagram" "https://www.instagram.com/fundacion.orientame/" 20 >}}
> Salud sexual y reproductiva con enfoque de derechos	

> {{< huglink "Movimiento Causa Justa" "https://causajustaporelaborto.org/" 20 >}},
> {{< huglink "Instagram" "https://www.instagram.com/causajustaporelaborto/" 20 >}}
> Causa Justa es un movimiento que busca la libertad y la autonomía reproductiva de todas las mujeres sobre sus cuerpos y sus proyectos de vida.

> {{< huglink "Fundación poderosas Colombia" "https://www.poderosascolombia.org/" 20 >}},
> {{< huglink "Instagram" "https://www.instagram.com/poderosascolombia/" 20 >}}
> ONG de educación integral para la sexualidad.

> {{< huglink "Fundación Mujer Despierta" "https://www.instagram.com/despiertafundacionmujer/" 20 >}}
> ONG que vela por los derechos de la infancia y mujeres en condición de vulnerabilidad

> {{< huglink "Coallacolectivallanera" "https://www.instagram.com/coallacolectivallanera/" 20 >}}
> Colectiva de acompañamiento en interrupción voluntaria del embarazo. Pedagogía en DSDR e IVE. Veeduría de seguimiento a la ruta IVE en Villavicencio.

> {{< huglink "Fundación Sergio Urrego" "https://www.instagram.com/fundacionsergiourrego/" 20 >}}
> Fundación en defensa de los derechos de la infancia y contra la discriminación sexual.

> {{< huglink "Mesa por la vida" "https://www.instagram.com/mesaporlavida/" 20 >}}
> ONG asesoria legal para mujeres que tienen dificultad para acceder al IVE 

## Legislación

> {{< huglink "Presenta nombre y breve descripción de cada Ley y complementarias" "https://sej.minjusticia.gov.co/ViolenciaGenero/Paginas/Contexto.aspx" 20 >}}
