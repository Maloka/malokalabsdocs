---
title:  "Explorando la tablet"
img: Tablet.png
categories: ["one", "two"]
date: "2021-09-15"
---

Te explicamos a continuación todas los componentes que vienen junto a la tableta gráfica.

## Exploración de tablet TOUCH 770G

![touch 770g](./touch770g-removebg-preview.png)


| Caracteríticas:              |  |
| ------------------- | ------------ |
| Marca         | TOUCH+      |
| Modelo        | 770G        |
| Pantalla | 7 Pulgadas |
| Resolución pantalla_ | 600 x 1024 |
| Memoria RAM         | 2 GB         |
| Almacenamiento      | 16 GB        |
| CPU | 4x ARM Cortex-A53 @ 1400 MHz |
 

**A continuación te compartimos el siguiente vídeo para que puedas realizar una exploración guiada de la tablet.**

{{< youtube 0QW87ooRcoA >}}

## Exploración de tablet y sus demás componentes

A continuación encontrarás la información de la tablet que fue usada en la implementación de
chicas STEAM 2021

{{< youtube xY7CZOaQLDk >}}

## Reinicio general de la tablet

El reinicio general de la tablet es útil cuando la tablet por alguna razón se bloquea y no quiere recibir ordenes
o cuando se quiere realizar un reinicio con los parámetros de fábrica.

{{< youtube x1Pz3EfICr8 >}}

## Activación de credenciales en la tablet para la plataforma Moodle de MalokaLABS y ChicaSTEAM.

{{< youtube AnDbglvm16A >}}
