---
title:  "Documentación Kit"
img: documentacion_kit.jpg
categories: ["one", "two"]
date: "2021-07-19"
---

## Documentación del Kit de Herramientas


{{< huglink "Documentación KiT y Aplicación MalokaLabs" "https://maloka.gitlab.io/malokalabsdocs/malokalabs-app/" 30 >}}

## Guías de uso sobre la caja de herramientas

{{< huglink "Guia de recursos" "https://view.genial.ly/60fa52c4b93d9e0da1c9553a/guide-guia-basica" 30>}}


