---
title:  "Hola Kit"
img: Luabot.png
categories: ["one", "two"]
date: "2021-07-21"
---

Sigue los pasos que encontrarás en el video para
que puedas hacer tu primer programa con la
tarjeta y Malokalabs, tendrás las instrucciones
e información de los materiales a usar.


## Hola mundo con Malokalabs

{{<youtube dFk3ZOOHFPA>}}


