---
title:  "Usar Zoom en Windows"
img: CopiaWindows.png
categories: ["one", "two"]
date: "2021-07-16"
---

Te vamos a presentar una ayudas que te pueden servir a mejorar tu experiencia en las sesiones sincrónicas en *Chicas Steam*, comencemos.

## ¿Cómo instalar zoom en Windows?

{{< youtube OSzbheItmbM >}}

## Cambiar nombre de Zoom en Windows

{{< youtube mpdOR2iuA6s >}}

## Activar micrófono y cámara en Zoom desde Windows

{{< youtube 9stzI4HaVwo >}}

## Levantar la mano y reaccionar en Zoom desde Windows

{{< youtube weB94TeMNHw >}}
