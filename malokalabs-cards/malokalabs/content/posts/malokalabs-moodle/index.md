---
title:  "MalokaLabs Moodle"
img: MalokaLbas_Moodle.jpg
categories: ["one", "two"]
date: "2022-05-16"
---


![malokalabs-img](malokalabsModle.png)

## Enlace a la plataforma de moodle

{{< huglink "https://steamnacional.malokalabs.org" "https://steamnacional.malokalabs.org" 30 >}}

## Tutorial de uso de la plataforma de moodle de MalokaLabs

{{< iframeGooglePres "https://docs.google.com/presentation/d/e/2PACX-1vS8Xgo-HJ1Vf16w1l_eXlJzEzTe9afaAke2cAssHMFN7rYof96dTBHCuAmki9P5asefCap5MuUP0K3Z/embed?start=false&loop=false&delayms=3000" >}}

{{< huglink "Enlace al tutorial" "https://docs.google.com/presentation/d/e/2PACX-1vS8Xgo-HJ1Vf16w1l_eXlJzEzTe9afaAke2cAssHMFN7rYof96dTBHCuAmki9P5asefCap5MuUP0K3Z/pub?start=false&loop=false&delayms=3000" 30 >}}



